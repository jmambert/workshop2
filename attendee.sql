DROP DATABASE workshop;
CREATE DATABASE workshop;
\c workshop

CREATE TABLE IF NOT EXISTS workshops (
    id serial primary key,
    title text,
    workshop_date date,
    location text,
    maxseats integer,
    instructor text,
    constraint fk_workshop unique (title, workshop_date, location)
);

CREATE TABLE IF NOT EXISTS users (
    userID serial NOT NULL,
    firstName text NOT NULL,
    lastName text NOT NULL,
    username text NOT NULL,
    email text NOT NULL,
    PRIMARY KEY (userID, firstName, lastName)
);
    
CREATE TABLE IF NOT EXISTS attendee (
    id serial primary key,
    userID serial,
    workshopID serial references workshops (id) on delete cascade,
    firstName text,
    lastName text,
    constraint fk_user
        foreign key(userID, firstName, lastName)
        references users (userID, firstName, lastName) on delete cascade
);

grant select, insert, delete on users, workshops, attendee to superuser;
grant usage on attendee_id_seq to superuser;
grant usage on users_userID_seq to superuser;
grant usage on workshops_id_seq to superuser;

/*
insert into workshops (name) values ('DevOps 101'),
                                    ('Docker Container Fundamentals'),
                                    ('Machine Learning'),
                                    ('MongoDB'),
                                    ('React Fundamentals'),
                                    ('Self-Driving Cars');


insert into attendee (firstName, lastName, workshopID) values ('Ahmed', 'Abdelali',1),
                                                              ('Ann', 'Frank', 2),
                                                              ('Ann', 'Frank', 3),
                                                              ('Ann', 'Mulkern', 1),
                                                              ('Ann', 'Mulkern', 2),
                                                              ('Clara', 'Weick', 3),
                                                              ('Clara', 'Weick', 4),
                                                              ('James', 'Archer', 6),
                                                              ('Linda','Park', 1),
                                                              ('Linda', 'Park', 2),
                                                              ('Linda', 'Park', 3),
                                                              ('Linda', 'Park', 4),
                                                              ('Linda', 'Park', 5),
                                                              ('Lucy', 'Smith', 3),
                                                              ('Lucy', 'Smith', 5),
                                                              ('Lucy', 'Smith', 6),
                                                              ('Roz', 'Billingsley', 4),
                                                              ('Samantha', 'Eggert', 1),
                                                              ('Samantha', 'Eggert', 2),
                                                              ('Samantha', 'Eggert', 3),
                                                              ('Samantha', 'Eggert', 4),
                                                              ('Samantha', 'Eggert', 5),
                                                              ('Samantha', 'Eggert', 6),
                                                              ('Tim', 'Smith', 2);
                             
*/

