const express = require('express');
var Pool = require('pg').Pool;
var bodyParser = require('body-parser');


const app = express();
var config = {
    host: 'localhost',
    user: 'superuser',
    password: 'ThisIsAPassword',
    database: 'workshop',
};

var pool = new Pool(config);

app.set('port', (8080));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));

Object.prototype.isEmpty = function() {
    for (var key in this) {
        if(this.hasOwnProperty(key))
            return false;
    }
    return true;
};
/*
app.get('/api', async (req, res) => {
    try {
        if (Object.keys(req.query).length === 0) {
            var anything = req.query.workshops;
            var response = await pool.query('select name from workshops');
            for (var i = 0; i < response.length; i++) {
            for (var key in response[i]) {
                if (response[i].hasOwnProperty(key)) {
                    console.log(response[i][key]);
                    }
                }
            }
        var listToPrint = new Array();
        response.rows.forEach( function (arrayItem) {
            listToPrint.push(arrayItem["name"]);
        });
        var dict = {
            workshops: listToPrint
        };
        res.json (dict);
        } else {
        var anything = req.query.workshop;
        var response = await pool.query('select a.firstname from attendee a join workshops w on a.workshopID = w.id where w.name = $1', [anything]);
        for (var i = 0; i < response.length; i++) {
        for (var key in response[i]) {
            if (response[i].hasOwnProperty(key)) {
                console.log(response[i][key]);
                }
            }
        }
        if (response.rows.isEmpty()) {
            res.json('error: workshop not found')
        } else {
        var listToPrint = new Array();
        response.rows.forEach( function (arrayItem) {
            listToPrint.push(arrayItem["firstname"]);
        });
             var dict = {
            attendee: listToPrint
        };
        res.json (dict);
            }
        }
    } catch(e) {
        console.error('Error running query $1 ', e.message)
    }
    
});

app.post('/api', async (req, res) => {
    var attendee = req.body.attendee
    var workshop = req.body.workshop;
    if (!attendee || !workshop) {
        res.json({error: 'parameters not given'});
    } else {
        var check = await pool.query('select a.firstname from attendee a join workshops w on a.workshopID = w.id where w.name = $1', [workshop]);
        if(check.rows.isEmpty()) {
            var table = await pool.query('insert into workshops (name) values ($1)', [workshop]);
            var person = await pool.query('insert into attendee (firstname, workshopid) values (($1),(SELECT id from workshops where name = ($2)))', [attendee, workshop]);
            var dict = {
                attendee: attendee,
                workshop: workshop
            }
            res.json(dict);
        } else {
            var checkers = await pool.query('select a.firstname from attendee a join workshops w on a.workshopID = w.id where a.firstname = $1 AND a.workshopID = (SELECT id from workshops WHERE name = ($2))', [attendee, workshop]);
            if (checkers.rows.isEmpty()) {
                var person = await pool.query('insert into attendee (firstname, workshopid) values (($1),(SELECT id from workshops where name = ($2)))', [attendee, workshop]);
                var dict = {
                    attendee: attendee,
                    workshop: workshop
                }
                res.json(dict);
            } else {
                res.json({error: 'attendee already enrolled'});
            }
        }
    }
});
*/
app.post('/create-user', async (req, res) => {
    //Load in parameters into variables
    try {
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;
    var username = req.body.username;
    var email = req.body.email;
    //Checks if all parameters are given
    if (!firstname || !lastname || !username || !email) {
        res.json({error: 'One or more parameters are missing'});
    } else {
        //Checks if it already exists in table
        var check = await pool.query('select firstname, lastname from users where firstname = $1 and lastname = $2', [firstname, lastname]);
        //If it is not already in the table
        if(check.rows.isEmpty()) {
            await pool.query('insert into users (firstname, lastname, username, email) values($1, $2, $3, $4)', [firstname, lastname, username, email]);
            res.json({status: 'added'});
        } 
        //If the entry already exists
        else {
            res.json({status: 'username taken'});
            }
        }
    
    } catch(e) {
        console.error('Error running query $1 ', e.message);
    }
});

app.delete('/delete-user', async (req, res) => {
    //Load in parameters into variables
    try {
    var username = req.query.username;
    //Checks if all parameters are given
    if (!username) {
        res.json({error: 'parameter not given'});
    } else {
            await pool.query('delete from users where username = $1', [username]);
            res.json({status: 'deleted'});
        }
    } catch(e) {
        console.error('Error running query $1 ', e.message);
    }
});

app.get('/list-users', async (req, res) => {
    //Load in parameters into variables
    var type = req.query.type;
    //Checks if all parameters are given
    if (type != 'summary' && type != 'full') {
        res.json({error: 'parameters are incorrect'});
    } else {
        //If it is not already in the table
        if (type == 'summary') {
            var list = await pool.query('select firstname, lastname from users');
            res.json({users: list.rows});
        } else {
            list = await pool.query('select firstname, lastname, username, email from users');
            res.json({users: list.rows});
        }
    }
});

app.post('/add-workshop', async (req, res) => {
    //Load in parameters into variables
    var title = req.body.title;
    var date = req.body.date;
    var location = req.body.location;
    var maxseats = req.body.maxseats;
    var instructor = req.body.instructor;
    //Checks if all parameters are given
    if (!title || !date || !location || !maxseats || !instructor) {
        res.json({error: 'One or more parameters are missing'});
    } else {
        //Checks if it already exists in table
        var check = await pool.query('select title, workshop_date, location from workshops where title = $1 and workshop_date = $2 and location = $3', [title, date, location]);
        //If it is not already in the table
        if(check.rows.isEmpty()) {
            await pool.query('insert into workshops (title, workshop_date, location, maxseats, instructor) values($1, $2, $3, $4, $5)', [title, date, location, maxseats, instructor]);
            res.json({status: 'workshop added'});
        } 
        //If the entry already exists
        else {
            res.json({status: 'workshop already in database'});
            }
        }
});

app.post('/enroll', async (req, res) => {
    //Load in parameters into variables
    var username = req.body.username;
    var title = req.body.title;
    var date = req.body.date;
    var location = req.body.location;
    //Checks if all parameters are given
    console.log(username, title, date, location);
    if (!username || !title || !date || !location) {
        res.json({error: 'One or more parameters are missing'});
    } else {
        //Checks if workshop exists
        var check = await pool.query('select title, workshop_date, location from workshops where title = $1 and workshop_date = $2 and location = $3', [title, date, location]);
        //If it is not already in the table
        if(check.rows.isEmpty()) {
            res.json({status: 'workshop does not exist'});
        } else {
            check = await pool.query('select username from users where username = $1', [username]);
            if (check.rows.isEmpty()) {
                res.json({status: 'user does not exist'});
            } else {
                var checkSeats = await pool.query('select maxseats from workshops where title = $1 and workshop_date = $2 and location = $3', [title, date, location]);
                var numSeatsTaken = await pool.query('select count(maxseats) as ms from attendee a JOIN workshops w ON a.workshopID = w.id where title = $1 and workshop_date = $2 and location = $3 GROUP BY a.workshopID', [title, date, location]);
                var seats = 0;
                if(!numSeatsTaken.rows.isEmpty()){
                    seats = numSeatsTaken.rows[0]["ms"];
                }
                if (checkSeats.rows[0]["maxseats"] == seats) {
                    res.json({status: 'no seats available'});
                } else {
                    await pool.query('insert into attendee (userID, workshopID, firstname, lastname) values ((select userID from users where username = $1), (select id from workshops where title = $2 and workshop_date = $3 and location = $4), (select firstname from users where username = $1), (select lastname from users where username = $1))',
                        [username, title, date, location]);
                    res.json({status: 'added'});
                }
            }
        }
    }
}); 

app.get('/attendees', async (req, res) => {
    //Load in parameters into variables
    var title = req.query.title;
    var date = req.query.date;
    var location = req.query.location;
    //Checks if all parameters are given
    if (!title || !date || !location) {
        res.json({error: 'parameters missing'});
    } else {
            var list = await pool.query('select a.firstname as firstname, a.lastname as lastname from attendee a join workshops w on w.id = a.workshopID where w.title = $1 and w.workshop_date = $2 and w.location = $3', [title, date, location]);
            res.json({attendees: list.rows});
    }
});

app.get('/list-workshops', async (req, res) => {
    var list = await pool.query('select title, to_char(workshop_date, \'yyyy-mm-dd\') as date, location from workshops');
    res.json({workshops: list.rows});
});

 //added extra #1
app.delete('/delete-workshop', async (req, res) => {
    //Load in parameters into variables
    try {
    var title = req.query.title;
    var date = req.query.date;
    var location = req.query.location;
    //Checks if all parameters are given
    if (!title || !date || !location) {
        res.json({error: 'parameters missing'});
    } else {
            await pool.query('delete from workshops where title = $1 and workshop_date = $2 and location = $3', [title, date, location]);
            res.json({status: 'deleted'});
        }
    } catch(e) {
        console.error('Error running query $1 ', e.message);
    }
});

//added extra #2
app.delete('/unenroll', async (req, res) => {
    //Load in parameters into variables
    try {
    var firstname = req.query.firstname;
    var lastname = req.query.lastname;
    var title = req.query.title;
    var date = req.query.date;
    var location = req.query.location;
    //Checks if all parameters are given
    if (!firstname || !lastname || !title || !date || !location) {
        res.json({error: 'parameters missing'});
    } else {
            await pool.query('delete from attendee where firstname = $1 and lastname = $2 and workshopID = (SELECT id from workshops WHERE title = $3 and workshop_date=$4 and location = $5)', [firstname, lastname, title, date, location]);
            res.json({status: 'unenrolled'});
        }
    } catch(e) {
        console.error('Error running query $1 ', e.message);
    }
});

//added extra #3
app.get('/locate-workshops', async (req, res) => {
    var location = req.query.location;
    if (!location) {
        res.json({error: 'parameters missing'});
    }
    var list = await pool.query('select title, to_char(workshop_date, \'yyyy-mm-dd\') as date, location from workshops where location = $1', [location]);
    res.json({workshops: list.rows});
});

app.listen(app.get('port'), () => {
    console.log('Running');
})